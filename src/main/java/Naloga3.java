import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Naloga3 {
	public static String input;
	public static int[] tabela1;
	public static int N, V, K, P;
	public static DLL<Integer>[] vrstice;

	/** Izračunana dolžina izhodnega besedila **/
	static int len;

	public static void main(String... args) throws FileNotFoundException {
		Scanner in = new Scanner(new File(args[0]));

		String[] split = in.nextLine().split(",");
		N = Integer.parseInt(split[0]);
		V = Integer.parseInt(split[1]);
		K = Integer.parseInt(split[2]);
		P = Integer.parseInt(split[3]);

		vrstice = new DLL[N];
		for (int i = 0; i < vrstice.length; i++)
			vrstice[i] = new DLL<Integer>();

		for (int i = 0; i < N; i++) {
			String line = in.nextLine();
			split = line.split(",");
			if (split[0].equals("")) continue;
			for (String s : split) {
				vrstice[i].append(Integer.parseInt(s));
			}
		}

		dekodiraj();

		PrintWriter output = new PrintWriter(args[1]);
		output.println(input);
		output.close();

	}

	public static void dekodiraj() {
		dekodiraj3();
		dekodiraj2();
		dekodiraj1();
	}

	public static void dekodiraj1() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
			sb.append(Mapper.decode(tabela1[i]));
		}
		input = sb.toString();
	}

	public static void dekodiraj2() {
		// Init
		len = 0;
		for (int i = 0; i < N; i++)
			len += vrstice[i].size();

		tabela1 = new int[len];
		for (int i = 0; i < tabela1.length; i++) {
			tabela1[i] = vrstice[i % N].popFirst();
		}
	}

	public static void dekodiraj3() {
		int row = V;
		for (int i = 0; i < K; i++) {
			DLL<Integer>.E e = vrstice[row].popLastRaw();
			// THANK YOU Java 8! //
			row = Math.floorMod(row - e.val, N); // (row + e.val) % N;
			vrstice[row].prepend(e);
			row = Math.floorMod(row - P, N); // (row + P) % N;
		}
	}

	/*
	 * Funkcije za kodiranje (za avtomatske teste)
	 */

	public static void kodiraj() {
		kodiraj1();
		kodiraj2();
		kodiraj3();
	}

	public static void kodiraj1() {
		tabela1 = new int[input.length()];
		for (int i = 0; i < input.length(); i++) {
			tabela1[i] = Mapper.code(input.charAt(i));
		}
	}

	public static void kodiraj2() {
		vrstice = new DLL[N];
		for (int i = 0; i < vrstice.length; i++)
			vrstice[i] = new DLL<Integer>();

		for (int i = 0; i < tabela1.length; i++) {
			vrstice[i % N].append(tabela1[i]);
		}

	}

	public static void kodiraj3() {
		int row = 0;
		for (int i = 0; i < K; i++) {
			DLL<Integer>.E e = vrstice[row].popFirstRaw();
			// THANK YOU Java 8! //
			row = Math.floorMod(row + e.val, N); // (row + e.val) % N;
			vrstice[row].append(e);
			row = Math.floorMod(row + P, N); // (row + P) % N;
		}
		V = Math.floorMod(row - P, N);
	}

	/**
	 *
	 */
	static class Mapper {
		// Generated using Excel
		static char[] chars = new char[] { 'A', 'B', 'C', 'Č', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
				'O', 'P', 'R', 'S', 'Š', 'T', 'U', 'V', 'Z', 'Ž', 'a', 'b', 'c', 'č', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 'š', 't', 'u', 'v', 'z', 'ž', ' ' };

		public static int code(char c) {
			// TODO: Faster search algorithm
			for (int i = 0; i < chars.length; i++) {
				if (chars[i] == c)
					return i;
			}
			throw new IllegalStateException("No mapping for char '" + c + "'");
		}

		public static char decode(int i) {
			return chars[i];
		}
	}

	/**
	 * Pretty bad implementation of a doubly-linked list, returning segments instead of
	 * values to allow efficient moving between lists.
	 * Many edge cases not handled!
	 * <br>
	 * IntelliJ Renderer:
	 *
	 *<pre>{@code
	 *ArrayList<Integer> list = new ArrayList<>();
	 *Naloga3.DLL<Integer>.E e = this.first;
	 *do {
	 *    list.add(e.val);
	 *} while ( (e = e.next) != null)
	 *list.toArray();
	 *}</pre>
	 */
	public static class DLL<T> {
		E first;
		E last;
		int size;

		/**
		 * Push raw value
		 */
		public void append(T val) {
			append(new E(null, val, null));
		}

		/**
		 * Push pre-wrapped value (saves a malloc() or two when moving entries between
		 * lists)
		 */
		public void append(E e) {
			e.next = null;
			if (first == null) {
				e.prev = null;
				first = e;
			} else {
				e.prev = this.last;
				last.next = e;
			}
			last = e;
			size++;
		}

		/**
		 * Push pre-wrapped value (saves a malloc() or two when moving entries between
		 * lists)
		 */
		public void prepend(E e) {
			e.prev = null;
			e.next = first;
			if (first != null) {
				first.prev = e;
			} else {
				last = e;
			}
			first = e;
			size++;
		}

		public E popFirstRaw() {
			if (first == null)
				return null;
			E tmp = this.first;
			this.first = tmp.next;
			if (this.first != null)
				this.first.prev = null;
			size--;
			return tmp;
		}

		public T popFirst() {
			return popFirstRaw().val;
		}

		public E popLastRaw() {
			if (this.last == null)
				return null;

			E tmp = this.last;
			// Last element
			if (last == first) {
				first = null;
				last = null;
			} else {
				last = tmp.prev;
				last.next = null;
			}
			size--;
			return tmp;
		}

		public T popLast() {
			return popLastRaw().val;
		}

		public int size() {
			return size;
		}

		/**
		 * Holds a DLL value and "pointers"
		 */
		public class E {
			public T val;
			public E next;
			public E prev;

			public E(E prev, T val, E next) {
				this.prev = prev;
				this.val = val;
				this.next = next;
			}
		}

	}
}
