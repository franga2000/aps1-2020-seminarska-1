import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Naloga1 {

    private static int[] razdaljeDo;
    private static int[] cene;
    private static int dolžinaPoti;

    private static int kapaciteta;
    private static int N;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File(args[0]));

        String[] split = in.nextLine().split(",");
        dolžinaPoti = Integer.parseInt(split[0]);
        kapaciteta = Integer.parseInt(split[1]);
        N = Integer.parseInt(split[2]);

        // Začetek in konec se smatrata ko pumpi
        // zato je vse zamaknjeno za 1 in podaljšano za 2
        cene = new int[N+2];
        razdaljeDo = new int[N+2];
        // Od tu se odštevajo vse razdalje, da dobimo razdaljo do konca (zadnje "pumpe")
        razdaljeDo[N+1] = dolžinaPoti;

        for (int i = 0; i < N; i++) {
            split = in.nextLine().split(":");
            split = split[1].split(",");
            razdaljeDo[i + 1] = Integer.parseInt(split[0]);
            razdaljeDo[N+1] -= razdaljeDo[i+1];
            cene[i + 1] = Integer.parseInt(split[1]);
        }

        PrintWriter output = new PrintWriter(args[1]);
        int[] pumpe = roadtrip();

        for (int i = 0; i < pumpe.length; i++) {
            output.print(pumpe[i] + (i != pumpe.length-1 ? "," : ""));
        }

        output.close();
    }

    public static int[] roadtrip() {
        // Za vsako pumpo (i) hrani predhodno pumpo (j), na kateri je najboljše tankat v primeru, da se ustavimo na (i)
        int[] bestPrevious = new int[N+2];
        for (int i = 0; i < N+1; i++)
            bestPrevious[i] = -1;

        // Poln (najnižji) strošek potovanja do pumpe
        int[] strošek = new int[N+2];

        /*
        Računanje stroškov za vse pumpe
         */

        for (int pumpa = 1; pumpa <= N+1; pumpa++) {
            int pot = 0;
            int best = Integer.MAX_VALUE;

            // Vse pumpe, od katerih lahko pridemo do te
            for (int prejšnjaPumpa = pumpa-1; prejšnjaPumpa >= 0; prejšnjaPumpa--) {
                pot += razdaljeDo[prejšnjaPumpa+1];
                if (pot > kapaciteta)
                    break;

                int plačilo = strošek[prejšnjaPumpa] + cene[pumpa]*pot;

                // strošek = strošek do prejšnje + kolko je treba dotankat tu (vmesna pot * cena)
                if ( plačilo <= best) {
                    best = plačilo;
                    bestPrevious[pumpa] = prejšnjaPumpa;

                }
            }

            strošek[pumpa] = best;
        }

        // Zadnja (fake) pumpa se ne šteje, zato vzamemo njeno predhodnjo
        int best_pumpa = bestPrevious[N+1];

        /*
         Zberi in obrni seznam postaj
         */

        DLL<Integer> list = new DLL<>();
        int pumpa = best_pumpa;
        while (pumpa > 0) {
            list.prepend(pumpa);
            pumpa = bestPrevious[pumpa];
        }

        int[] postaje = new int[list.size()];

        DLL<Integer>.E e = list.first;
        int i = 0;
        do {
            postaje[i++] = e.val;
        } while ( (e = e.next) != null);

        return postaje;
    }



    /**
     * Pretty bad implementation of a doubly-linked list, returning segments instead of
     * values to allow efficient moving between lists.
     * Many edge cases not handled!
     * <br>
     * IntelliJ Renderer:
     *
     *<pre>{@code
     *ArrayList<Integer> list = new ArrayList<>();
     *Naloga3.DLL<Integer>.E e = this.first;
     *do {
     *    list.add(e.val);
     *} while ( (e = e.next) != null)
     *list.toArray();
     *}</pre>
     */
    public static class DLL<T> {
        E first;
        E last;
        private int size;

        /**
         * Push pre-wrapped value (saves a malloc() or two when moving entries between
         * lists)
         */
        public void prepend(E e) {
            e.prev = null;
            e.next = first;
            if (first != null) {
                first.prev = e;
            } else {
                last = e;
            }
            first = e;
            size++;
        }

        public void prepend(T v) {
            prepend(new E(null, v, null));
        }

        public int size() {
            return size;
        }

        /**
         * Holds a DLL value and "pointers"
         */
        public class E {
            public T val;
            public E next;
            public E prev;

            public E(E prev, T val, E next) {
                this.prev = prev;
                this.val = val;
                this.next = next;
            }
        }

    }

}
