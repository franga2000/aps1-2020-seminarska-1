import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Naloga5 {
    
    public static PrintWriter output = new PrintWriter(System.out);
    
    public static int N;
    /** Zmanjšan za 1 **/
    public static int P;
    private static char[][] začetna;
    private static char[][] končna;

    public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner(new File(args[0]));
		
		String[] split = in.nextLine().split(",");
		P = Integer.parseInt(split[0]);
		N = Integer.parseInt(split[1]);
        začetna = new char[P][N];
        končna = new char[P][N];

        readState(in, začetna);
        readState(in, končna);

        output = new PrintWriter(args[1]);

        open = new BadPriorityQueue<>();
        closed = new BadPriorityQueue<>();
		Konfiguracija[] moves = solve(začetna, končna);

		for (Konfiguracija konf : moves) {
		    output.print("VZEMI ");
		    output.println(konf.moveFrom+1);
            output.print("IZPUSTI ");
            output.println(konf.moveTo+1);
        }
		
		output.close();
	}


	// Tu še vedno piše "Priority" ampak je v bistvu uporabljen samo kot navadni queue.
    //
	static BadPriorityQueue<Konfiguracija> open;
    static BadPriorityQueue<Konfiguracija> closed;

	public static Konfiguracija[] solve(final char[][] ZAČETNA, final char[][] KONČNA) {
        open.addSorted(new Konfiguracija(ZAČETNA, null, 0, -1, -1));
        Konfiguracija target = new Konfiguracija(KONČNA, null, Integer.MAX_VALUE, -1, -1);


	    //while (open.size() > 0) { // Predpostavljam, da obstaja rešitev
	    while (true) {

	        // pop
	        Konfiguracija current = open.popFirst();

            if (current.equals(target)) {
                // This one is better
                if (current.compareTo(target) < 0) {
                    target = current;
                    break;
                }
                continue;
            }

            Set<Konfiguracija> next = new Set<>();
            current.dodajNaslednje(next);

            // Vsak potencialni state
            Set<Konfiguracija>.SetElement e = next.first();
            while ( (e = e.next) != null) {
                Konfiguracija considering = e.element;
                if (closed.contains(considering)) {
                    continue;
                }

                Konfiguracija best_considering = open.find(considering);
                if (best_considering != null) {
                    // Override current best
                    if (considering.compareTo(best_considering) < 0) {
                        open.remove(best_considering);
                        open.append(considering);
                    }
                } else {
                    open.append(considering);
                }
            }

            closed.addOnce(current);

        }

        return backtrack(target);

    }

    static Konfiguracija[] backtrack(Konfiguracija target) {
	    Konfiguracija[] backtrack = new Konfiguracija[target.globina];
	    Konfiguracija konf = target;
        for (int i = target.globina-1; i >= 0; i--) {
	        backtrack[i] = konf;
	        konf = konf.prejšnja;
        }
	    return backtrack;
    }


    static class Konfiguracija implements Comparable<Konfiguracija> {
	    char[][] škatle;
	    Konfiguracija prejšnja;
	    int globina;
	    int moveFrom;
	    int moveTo;
	    int hashCode;

	    // Queue stuff
        // Sem mislu da bom par malloc() prišparal če ne uporabim element wrapper classa, pol pa sem itak dodal še Set pa HashMap vsak s svojimi wrapperi, so I guess that was for nothing...
        Konfiguracija prev;
        Konfiguracija next;

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(moveFrom).append(" -> ").append(moveTo).append(" @ ").append(globina).append('\n');
            for (char[] stack : škatle) {
                sb.append(stack).append('\n');
            }
            return sb.toString();
        }

        public Konfiguracija(Konfiguracija prejšnja, int globina, int moveFrom, int moveTo) {
            this.prejšnja = prejšnja;
            this.globina = globina;
            this.moveFrom = moveFrom;
            this.moveTo = moveTo;
        }

        public Konfiguracija(char[][] škatle, Konfiguracija prejšnja, int globina, int moveFrom, int moveTo) {
            this.škatle = škatle;
            this.prejšnja = prejšnja;
            this.globina = globina;
            this.moveFrom = moveFrom;
            this.moveTo = moveTo;
            prehash();
        }

        private void prehash() {
            this.hashCode = hash(this.škatle);
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

        public int dodajNaslednje(Set<Konfiguracija> naslednje) {
            //Set<Konfiguracija> naslednje = new HashSet<Konfiguracija>();
            int i = 0;
            for (int from = 0; from < škatle.length; from++) {
                for (int to = 0; to < škatle.length; to++) {

                    if (from == to) // Premik na isto mesto
                        continue;
                    if (from == moveTo) // Premik, ki nadaljuje prejšnji premik
                        continue;
                    if (!canMove(škatle, from, to)) // Premik ni mogoč
                        continue;
                    //if (škatle[from][1] == 0 && škatle[to][0] == 0) // Premik edine škatle na prazno mesto
                    //    continue;
                    if (isDone(škatle, from))
                        continue;

                    Konfiguracija naslednja = this.movedClone(from, to);
                    naslednje.add(naslednja);
                    i++;
                }
            }
            //return naslednje;
            return i;
        }

        private boolean isDone(char[][] škatle, int from) {
            for (int i = 0; i < škatle[from].length; i++) {
                if (škatle[from][i] != končna[from][i])
                    return false;
            }
            return true;
        }

        private Konfiguracija movedClone(int from, int to) {
            char[][] new_škatle = new char[škatle.length][škatle[0].length];
            arrayCopy(this.škatle, new_škatle);
            Konfiguracija konf = new Konfiguracija(this, globina+1, from, to);
            konf.škatle = new_škatle;
            konf.move(from, to);
            konf.prehash();
            return konf;
        }

        void move(int from, int to) {
            int nFrom = findTop(from, false);
            int nTo = findTop(to, true);

            škatle[to][nTo] = škatle[from][nFrom];
            škatle[from][nFrom] = 0;
        }

        int findTop(int stack, boolean space) {
            int firstZero = -1;
            for (int i = 0; i < škatle[stack].length; i++) {
                if (škatle[stack][i] == 0) {
                    firstZero = i;
                    break;
                }
            }
            if (firstZero == -1)
                firstZero = škatle[stack].length;
            return space ? firstZero : firstZero - 1;
        }

        public boolean equals(Object obj) {
            if (! (obj instanceof Konfiguracija))
                return false;
            Konfiguracija other = (Konfiguracija) obj;
            boolean fast = this.hashCode == other.hashCode;
            return fast && slowEquals(other);
            //boolean slow = slowEquals(other);
            //return slow;
        }
        public boolean slowEquals(Konfiguracija other) {
            for (int i = 0; i < škatle.length; i++) {
                for (int j = 0; j < škatle[i].length; j++) {
                    if (škatle[i][j] != other.škatle[i][j])
                        return false;
                }
            }
            return true;
        }

        @Override
        public int compareTo(Konfiguracija other) {
            return globina - other.globina;
        }
    }


    /**
     * Frankenstein class ki deluje kot PriorityQueue, ampak omogoča tudi hiter dostop izven vrstnega reda s pomočjo HashMap "indeksa"
     * <br>
     * IntelliJ Renderer:
     *
     *<pre>{@code
     *ArrayList<Integer> list = new ArrayList<>();
     *Naloga3.DLL<Integer>.E e = this.first;
     *do {
     *    list.add(e.val);
     *} while ( (e = e.next) != null)
     *list.toArray();
     *}</pre>
     */
    static class BadPriorityQueue<T> {

        Konfiguracija first;
        Konfiguracija last;
		//int size;
		HashMap<Konfiguracija, Konfiguracija> hashMap;
//		HashMap<Integer, Konfiguracija> globinaMap;

		public BadPriorityQueue() {
            hashMap = new HashMap<>();
//            globinaMap = new HashMap<>(100);
        }

		/**
		 * Push pre-wrapped value (saves a malloc() or two when moving entries between
		 * lists)
		 */
		public void append(Konfiguracija e) {
			e.next = null;
			if (first == null) {
				e.prev = null;
				first = e;
			} else {
				e.prev = this.last;
				last.next = e;
			}
			last = e;
			//size++;
			hashMap.put(e,e);
		}

		public Konfiguracija popFirst() {
			if (first == null)
				return null;
            Konfiguracija tmp = this.first;
			this.first = tmp.next;
			if (this.first != null)
				this.first.prev = null;
			//size--;
            hashMap.remove(tmp);
 //           if (globinaMap.get(tmp.globina) == tmp)
 //               globinaMap.put(tmp.globina, tmp.prev);
			return tmp;
		}


		public Konfiguracija get(int index) {
            Konfiguracija k = this.first;
            for (int i = 0; i < index; i++) {
                k = k.next;
            }
            return k;
        }

        public Konfiguracija find(Konfiguracija needle) {
		    Konfiguracija fast = hashMap.get(needle);
		    return fast;
        }

        public Konfiguracija slowFind(Konfiguracija needle) {
            Konfiguracija k = this.first;
            if (k == null)
                return null;
            do {
                if (needle.equals(k))
                    return k;
            } while ( (k = k.next) != null);
            return null;
        }

		//public int size() {
		//	return size;
		//}

        public void remove(Konfiguracija konf) {
		    if (konf.prev == null) {
		        this.first = konf.next;
            } else {
		        konf.prev.next = konf.next;
            }

		    if (konf.next == null) {
		        this.last = konf.prev;
            } else {
		        konf.next.prev = konf.prev;
            }

//            if (globinaMap.get(konf.globina) == konf)
///                globinaMap.put(konf.globina, konf.prev);

		    //size -= 1;
        }

        public void addSorted(Konfiguracija adding) {
            hashMap.put(adding,adding);
		    Konfiguracija before = this.first;
            //size += 1;

		    // V teoriji skrajša iskanje
            if (adding.prejšnja != null && adding.prejšnja.globina > 0)
                before = adding.prejšnja;

 //           Konfiguracija closest = globinaMap.get(adding.globina);
 //           if (closest != null)
 //               before = closest;

		    // Only element
		    if (before == null) {
		        this.first = adding;
		        this.last = adding;
		        adding.prev = null;
		        adding.next = null;
		        return;
            }
		    // Insert before
		    do {
		        // New one is better
                if (adding.compareTo(before) < 0) {
                    adding.next = before;
                    if (adding.prev != null) {
                        adding.prev.next = adding;
                    } else {
                        this.first = adding;
                    }
                    before.prev = adding;
                    return;
                }
            } while ( (before = before.next) != null);
            // Last element
            this.last.next = adding;
            adding.prev = this.last;
            this.last = adding;
            adding.next = null;
            hashMap.put(adding,adding);
 //           if (closest == null)
 //               globinaMap.put(adding.globina, adding);
        }

        public boolean contains(Konfiguracija needle) {
		    return find(needle) != null;
        }

        public void addOnce(Konfiguracija needle) {
		    if (!contains(needle))
                append(needle);
        }
    }

    public static void arrayCopy(char[][] aSource, char[][] aDestination) {
        for (int i = 0; i < aSource.length; i++) {
            System.arraycopy(aSource[i], 0, aDestination[i], 0, aSource[i].length);
        }
    }


    public static boolean canMove(char[][] konfiguracija, int from, int to) {
        // Ni prostora
        if (konfiguracija[to][konfiguracija[to].length-1] != 0)
            return false;
        // Ni škatle
        if (konfiguracija[from][0] == 0)
            return false;

        return true;
    }


    public static int slowhash(char[][] konf) {
        int hash = 7;
        for (char[] stack : konf) {
            hash = 31 * hash * Arrays.hashCode(stack);
        }
        return hash;
    }

    // HOW is this stringy bullshit faster than the one above?!?
    public static int hash(char[][] konfiguracija) {
        StringBuilder s = new StringBuilder();
        for (char[] chars : konfiguracija) {
            s.append(chars);
        }
        return s.toString().hashCode();
    }


    private static void readState(Scanner in, char[][] konfiguracija) {
        String[] split;
        for (int i = 0; i < P; i++) {
            split = in.nextLine().split(":");
            if (split.length < 2) // Empty
                continue;
            int p = Integer.parseInt(split[0])-1;
            char[] chars = split[1].replace(",", "").toCharArray();
            System.arraycopy(chars, 0, konfiguracija[p], 0, chars.length);
        }
    }

    public static class HashMap<K, V> {

        // In case it's not obvious, this is where I got a bit desperate for a speedup...
        public static final int DEFAULT_SIZE = 999999;

        Set<HashMapNode>[] table;

        public HashMap() {
            makenull(DEFAULT_SIZE);
        }

        public HashMap(int size) {
            makenull(size);
        }

        public void makenull() {
            makenull(DEFAULT_SIZE);
        }

        public void makenull(int size) {
            table = new Set[size];

            for (int i = 0; i < table.length; i++) {
                table[i] = new Set();
            }
        }

        private int hash(K d) {
            return Math.abs(d.hashCode()) % table.length;
        }

        public void put(K d, V r) {
            Set<HashMapNode> l = table[hash(d)];
            HashMapNode node = new HashMapNode(d, r);

            Set<HashMapNode>.SetElement pos = l.locate(node);

            if (pos != null)
                l.retrieve(pos).setValue(r);
            else
                l.add(node);
        }

        public V get(K d) {
            Set<HashMapNode> l = table[hash(d)];

            Set<HashMapNode>.SetElement pos = l.locate(new HashMapNode(d, null));

            if (pos != null)
                return l.retrieve(pos).getValue();

            return null;
        }

        public void remove(K d) {
            Set<HashMapNode> l = table[hash(d)];
            Set<HashMapNode>.SetElement pos = l.locate(new HashMapNode(d, null));

            if (pos != null)
                l.delete(pos);
        }

        class HashMapNode {
            K key;
            V value;

            public HashMapNode(K key, V value) {
                this.key = key;
                this.value = value;
            }

            public V getValue() {
                return value;
            }
            public void setValue(V value) {
                this.value = value;
            }

            @SuppressWarnings("unchecked")
            @Override
            public boolean equals(Object obj) {
                if (!(obj instanceof HashMap.HashMapNode))
                    return false;
                HashMapNode other = (HashMapNode) obj;
                return key.equals(other.key);
            }

        }

    }


    public static class Set<T> {
        class SetElement {
            T element;
            SetElement next;

            SetElement() {
                element = null;
                next = null;
            }
        }

        private SetElement first;

        public Set() {
            makenull();
        }

        public void makenull() {
            first = new SetElement();
        }

        public SetElement first() {
            return first;
        }

        public SetElement next(SetElement pos) {
            return pos.next;
        }

        public void add(T obj) {
            // nov element vstavimo samo, ce ga ni med obstojecimi elementi mnozice
            // TODO: check if this matters
            //if (locate(obj) == null) {
                SetElement nov = new SetElement();
                nov.element = obj;
                nov.next = first.next;
                first.next = nov;
            //}
        }

        public void delete(SetElement pos) {
            pos.next = pos.next.next;
        }

        public boolean isLast(SetElement pos) {
            if (pos.next == null)
                return true;
            else
                return false;
        }

        public boolean empty() {
            return first.next == null;
        }

        public T retrieve(SetElement pos) {
            return pos.next.element;
        }

        public SetElement locate(T obj) {
            // sprehodimo se cez seznam elementov in preverimo enakost (uporabimo metodo equals)
            for (SetElement iter = first(); !isLast(iter); iter = next(iter))
                if (obj.equals(retrieve(iter)))
                    return iter;

            return null;
        }

    }


}
