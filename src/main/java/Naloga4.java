import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Naloga4 {

	public static void main(String[] args) throws FileNotFoundException {
        Naloga4 mem = new Naloga4();

		Scanner in = new Scanner(new File(args[0]));
		
        int N = in.nextInt();
        
        in.nextLine(); // Drop the \n
        
        for (int i = 0; i < N; i++) {
            String line = in.nextLine();
            String[] cmd = line.split(",");
            switch (cmd[0].charAt(0)) {
                case 'i':
                    mem.init(Integer.parseInt(cmd[1]));
                    break;
                case 'f':
                    mem.free(Integer.parseInt(cmd[1]));
                    break;
                case 'd':
                    mem.defrag(Integer.parseInt(cmd[1]));
                    break;
                case 'a':
                    mem.alloc(Integer.parseInt(cmd[1]), Integer.parseInt(cmd[2]));
                    break;
            }
        }

		PrintWriter output = new PrintWriter(args[1]);

        mem.dump(output);
		
		output.close();
	}

    public void dump(PrintWriter output) {
        Block block = firstBlock;
        do {
            output.println(block);
        } while ( (block = block.next) != null );
    }

    //public byte[] memory;

    int memSize;
    public Block firstBlock;
    
    public void init(int size) {
        this.memSize = size;
        firstBlock = null;
        //memory = new byte[size];
    }
    
    // Če bi se le spomnu uporabit implementacijo DLL od prejšnje naloge......
    // THIS is why there's a standard library!
    public boolean alloc(int size, int id) {
        // Freshly initialized special case
        if (firstBlock == null) {
            if (memSize < size)
                return false;
            firstBlock = new Block(null, id, 0, size-1, null);
            return true;
        }

        Block blockFrom;
        int from = 0;

        if (firstBlock.from >= size) // Gap before first block
            blockFrom = null;
        else {
            blockFrom = findGap(size); // Find more gaps
            // Couldn't find any
            if (blockFrom == null)
                return false;
            
            from = blockFrom.to+1;
        }

        Block newBlock = new Block(null, id, from, from+size-1, null);
        
        // New first block
        if (blockFrom == null) {
            newBlock.next = firstBlock;
            firstBlock.prev = newBlock;
            firstBlock = newBlock;
        } 
        // Insert after blockFrom
        else {
            newBlock.prev = blockFrom;
            
            if (blockFrom.next != null) {
                newBlock.next = blockFrom.next;
                blockFrom.next.prev = newBlock;
            }
            blockFrom.next = newBlock;
        }
        
        return true;
    }

    /**
     * DOES NOT FIND A GAP BEFORE THE FIRST ELEMENT!
     * (because there's no way to return that info)
     * Thanks, Java!
     */
    public Block findGap(int size) {        
        Block block = firstBlock;
        while (block.next != null) {
            if (block.next.from - block.to > size) {
                return block;
            }
            block = block.next;
        }

        // Last block special case
        if (memSize - block.to > size)
            return block;

        // No gaps found
        return null;
    }

    public int free(int id) {
        Block block = findBlock(id);
        if (block == null)
            return 0;
        
        if (block.prev == null) {
            firstBlock = block.next;
        } else {
            block.prev.next = block.next;
        }
        if (block.next != null) {
            block.next.prev = block.prev;
        }
        return block.to - block.from;
    }
    
    public void defrag(int n) {
        Block defragged = null;
        for (int i = 0; i < n; i++) {
            Block block = defragged == null ? firstBlock : defragged;
            defragged = null;
            do {
                int gap = block.from;
                if (block.prev != null)
                    gap = block.from - block.prev.to - 1;
                
                if (gap > 0) {
                    block.from -= gap;
                    block.to -= gap;
                    defragged = block;
                    break;
                }
            } while ( (block = block.next) != null );

            if (defragged == null)
                return;
        }
    }
    
    public Block findBlock(int id) {
        Block block = firstBlock;
        do {
            if (block.id == id) {
                return block;
            }
        } while ( (block = block.next) != null );
        return null;
    }
    

    public static class Block {
        public Naloga4.Block prev;
        public int id;
        public int from;
        public int to;
        public Naloga4.Block next;

        public Block(Block prev, int id, int from, int to, Block next) {
            this.prev = prev;
            this.id = id;
            this.from = from;
            this.to = to;
            this.next = next;
        }

        @Override
        public String toString() {
            return id + "," + from + "," + to;
        }

        /*
        IntelliJ renderer:
            ArrayList<Naloga4.Block> list = new ArrayList<Naloga4.Block>();
            Naloga4.Block block = this;
            do {
                list.add(block);
            } while ( (block = block.next) != null );
            list.toArray();
         */
    }
}
