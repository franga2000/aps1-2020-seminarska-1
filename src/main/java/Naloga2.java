import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


/**
 * Naloga2
 */
public class Naloga2 {

	public static PrintWriter output = new PrintWriter(System.out);

	public static void main(String[] args) throws FileNotFoundException {
		Scanner in = new Scanner(new File(args[0]));
		
		String[] split = in.nextLine().split(",");
		int Y = Integer.parseInt(split[0]);
		int X = Integer.parseInt(split[1]);
		char[][] polje = new char[Y][X];

		int y = 0;
		while (in.hasNext()) {
			polje[y++] = in.nextLine().replace(",", "").toCharArray();
		}

		output = new PrintWriter(args[1]);


		Naloga2.searchAll(polje);
		
		output.close();
	}

	public enum Smer {
		LEVO, DESNO, GOR, DOL
	}

	public static int[][] visited;

	public static void searchAll(char[][] polje) {
		int maxPot_len = -1;
		int maxStartX = -1;
		int maxStartY = -1;
		LinkedList<Smer> maxPot = null;

		for (int y = 0; y < polje.length; y++) {
			for (int x = 0; x < polje[0].length; x++) {
				visited = new int[polje.length][polje[0].length];

				LinkedList<Smer> pot = new LinkedList<>();

				int pot_len = search(polje, pot, x, y, 1);
				if (pot_len > maxPot_len) {
					maxPot_len = pot_len;
					maxPot = pot;
					maxStartX = x;
					maxStartY = y;
				}
			}
		}

		// IZPIS

		output.println(maxStartY + "," + maxStartX);

		LinkedList<Smer>.Entry smer = maxPot.first;
		output.print(smer.value);

		while ( (smer = smer.next) != null ) {
			output.print(",");
			output.print(smer.value);
		}

	}

	public static int search(char[][] polje, LinkedList<Smer> pot, int x, int y, int runPos) {
		visited[y][x] = runPos;

		LinkedList<Smer> testPot;
		Smer best = null;
		LinkedList<Smer> bestPot = null;

		int poskus; // Does this actually matter in Java?
		int max = runPos;
		int tmpVisited;
		if (canMove(polje, polje[y][x], x+1, y) && notVisited(x + 1, y, runPos)) {
			testPot = new LinkedList<>();
			tmpVisited = visited[y][x+1];
			poskus = search(polje, testPot, x+1, y, runPos+1);
			visited[y][x+1] = tmpVisited;
			if (poskus > max) {
				best = Smer.DESNO;
				bestPot = testPot;
				max = poskus;
			}
		}
		if (canMove(polje, polje[y][x], x-1, y) && notVisited(x - 1, y, runPos)) {
			testPot = new LinkedList<>();
			tmpVisited = visited[y][x-1];
			poskus = search(polje, testPot, x-1, y, runPos+1);
			visited[y][x-1] = tmpVisited;
			if (poskus > max) {
				best = Smer.LEVO;
				bestPot = testPot;
				max = poskus;
			}
		}
		if (canMove(polje, polje[y][x], x, y-1) && notVisited(x, y - 1, runPos)) {
			testPot = new LinkedList<>();
			tmpVisited = visited[y-1][x];
			poskus = search(polje, testPot, x, y-1, runPos+1);
			visited[y-1][x] = tmpVisited;
			if (poskus > max) {
				best = Smer.GOR;
				bestPot = testPot;
				max = poskus;
			}
		}
		if (canMove(polje, polje[y][x], x, y+1) && notVisited(x, y + 1, runPos)) {
			testPot = new LinkedList<>();
			tmpVisited = visited[y+1][x];
			poskus = search(polje, testPot, x, y+1, runPos+1);
			visited[y+1][x] = tmpVisited;
			if (poskus > max) {
				best = Smer.DOL;
				bestPot = testPot;
				max = poskus;
			}
		}

		// Nowhere to move from here
		if (max == runPos) {
			return runPos;
		} else {
			pot.add(best);
			pot.addAll(bestPot);

			return max;
		}
	}

	private static boolean notVisited(int x, int y, int runPos) {
		if (visited[y][x] == 0)
			return true;
		return visited[y][x] >= runPos;
	}

	public static boolean canMove(char[][] polje, char ch, int x, int y) {
		if (y >= polje.length || y < 0)
			return false;
		if (x >= polje[0].length || x < 0)
			return false;
		
		return polje[y][x] == ch;
	}

	/**
	 * Improvizirana implementacija single-linked lista
	 * (z optimizirano združitvijo - unlike java.util.List !?!)
	 */
	static class LinkedList<T> {
		Entry first;
		Entry last;

		public void add(T value) {
			Entry entry = new Entry(value);
			if (first == null) {
				first = entry;
			} else {
				last.next = entry;
			}
			last = entry;
		}

		public void addAll(LinkedList<T> other) {
			this.last.next = other.first;
			this.last = other.last;
		}

		class Entry {
			Entry next;
			T value;
			public Entry(T value) {
				this.value = value;
			}
		}
	}

}
