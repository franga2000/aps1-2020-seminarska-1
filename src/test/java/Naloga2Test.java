import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;


public class Naloga2Test {

	@Test
	public void testSearch() {
		char[][] polje = {
			{'X','X','X','X'},
			{'X','X','H','H'},
			{'H','O','O','O'},
			{'H','X','H','O'},
			{'H','H','O','O'}
		};
		
		//Naloga2.Smer[] pot = new Naloga2.Smer[512];
		Naloga2.LinkedList<Naloga2.Smer> pot = new Naloga2.LinkedList<>();
		Naloga2.visited = new int[polje.length][polje[0].length];

		int dolžina = Naloga2.search(polje, pot, 0, 0, 1);
		
		StringBuilder out = new StringBuilder();
		int i = 1;
		Naloga2.LinkedList<Naloga2.Smer>.Entry smer = pot.first;
		out.append(smer.value);
		while ( (smer = smer.next) != null ) {
			out.append(",");
			out.append(smer.value);
		}

		assertEquals("DOL,DESNO,GOR,DESNO,DESNO", out.toString());
		assertEquals(6, dolžina);
	}

	@Test
	public void testSearchAll() {
		char[][] polje = {
			{'X','X','X','X'},
			{'X','X','H','H'},
			{'H','O','O','O'},
			{'H','X','H','O'},
			{'H','O','O','O'}
		};
		
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		//final PrintStream originalOut = System.out;
		//System.setOut(new PrintStream(outContent));
		Naloga2.output = new PrintWriter(outContent);

		Naloga2.searchAll(polje);

		Naloga2.output.flush();
		String out = outContent.toString();

		assertEquals("2,1", out.split("\n")[0]);
		assertEquals("DESNO,DESNO,DOL,DOL,LEVO,LEVO", out.split("\n")[1]);
		//System.setOut(originalOut);
	}

	@TestFactory
	Collection<DynamicTest> testniPrimeri() {
		APSTest aps = new APSTest(Naloga2.class, "./Testni primeri/2/", 2);
		return aps.genTests();
	}

}
