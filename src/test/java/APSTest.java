import org.junit.jupiter.api.DynamicTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class APSTest {
    Class<?> cls;
    String testFolder;
    int naloga;
    private Method mainMethod;
    public String ext = ".txt";


    public APSTest(Class<?> cls, String testFolder, int naloga) {
        this.cls = cls;
        this.testFolder = testFolder;
        this.naloga = naloga;
        try {
            this.mainMethod = this.cls.getMethod("main", String[].class);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new RuntimeException("This should not happen!", e);
        }
    }

    public int numTests() {
        int i = 1;
        while (true) {
            File inFile = new File(testFile("I", i));
            if (!inFile.isFile()) {
                return i-1;
            }
            i++;
        }

    }

    public Collection<DynamicTest> genTests() {
        // Functional programming in Java, huh?
        return IntStream.range(1, this.numTests() + 1).mapToObj(
                (i) -> DynamicTest.dynamicTest("Testni primer " + i,
                        () -> this.test(i)
                )
        ).collect(Collectors.toList());
    }

    public void test(int test) throws Throwable {
        System.out.println("Test " + test);
        File inFile = new File(testFile("I", test));
        String tmpFileName = testFile("T", test);

        String[] args = new String[] {inFile.getPath(), tmpFileName};

        try {
            this.mainMethod.invoke(null, (Object) args);
        } catch (IllegalAccessException | IllegalArgumentException | SecurityException e) {
            throw new RuntimeException("Your code is bad!", e);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
        
        Scanner expected = new Scanner(new File(testFile("O", test)));
        Scanner actual = new Scanner(new File(tmpFileName));

        int line = 0;
        while (expected.hasNext()) {
            assertEquals(expected.nextLine(), actual.nextLine(), "Mismatch on line " + ++line);
        }
        expected.close();
        actual.close();
    }

    String testFile(String prefix, int test) {
        return testFolder + "/" + prefix + naloga + "_" + test + this.ext;
    }
    
}
