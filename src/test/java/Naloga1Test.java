import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Naloga1Test {

    @TestFactory
    Collection<DynamicTest> testniPrimeri() {
        APSTest aps = new APSTest(Naloga1.class, "./Testni primeri/1/", 1);
        return aps.genTests();
    }

}
