import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Naloga5Checker {
    public static int N;
    public static int P;
    private static char[][] začetna;
    private static char[][] končna;

    public static void main(String[] args) throws FileNotFoundException {
        boolean good = test(args[0], args[1]);
        System.out.println(good);

    }

    public static boolean test(String input, String output) throws FileNotFoundException {
        Scanner in = new Scanner(new File(input));

        String[] split = in.nextLine().split(",");
        P = Integer.parseInt(split[0]);
        N = Integer.parseInt(split[1]);
        začetna = new char[P][N];
        končna = new char[P][N];

        readState(in, začetna);
        readState(in, končna);

        Scanner out = new Scanner(new File(output));

        String[] line;
        while (out.hasNext()) {
            line = out.nextLine().split(" ");
            int from = Integer.parseInt(line[1]) - 1;
            line = out.nextLine().split(" ");
            int to = Integer.parseInt(line[1]) - 1;
            move(začetna, from, to);
        }

        return stateEquals(začetna, končna);
    }

    public static boolean stateEquals(char[][] škatle, char[][] other) {
        for (int i = 0; i < škatle.length; i++) {
            for (int j = 0; j < škatle[i].length; j++) {
                if (škatle[i][j] != other[i][j])
                    return false;
            }
        }
        return true;
    }

    static void move(char[][] škatle, int from, int to) {
        int nFrom = findTop(škatle, from, false);
        int nTo = findTop(škatle, to, true);

        škatle[to][nTo] = škatle[from][nFrom];
        škatle[from][nFrom] = 0;
    }

    static int findTop(char[][] škatle, int stack, boolean space) {
        int firstZero = -1;
        for (int i = 0; i < škatle[stack].length; i++) {
            if (škatle[stack][i] == 0) {
                firstZero = i;
                break;
            }
        }
        if (firstZero == -1)
            firstZero = škatle[stack].length;
        return space ? firstZero : firstZero - 1;
    }


    private static void readState(Scanner in, char[][] konfiguracija) {
        String[] split;
        for (int i = 0; i < P; i++) {
            split = in.nextLine().split(":");
            if (split.length < 2) // Empty
                continue;
            int p = Integer.parseInt(split[0]) - 1;
            char[] chars = split[1].replace(",", "").toCharArray();
            System.arraycopy(chars, 0, konfiguracija[p], 0, chars.length);
        }
    }
}