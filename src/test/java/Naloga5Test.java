import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

public class Naloga5Test {
    APSTest aps = new APSTest(Naloga5.class, "./Testni primeri/5/", 5);

    @TestFactory
    Collection<DynamicTest> testniPrimeri() {
            // Functional programming in Java, huh?
            return IntStream.range(1, aps.numTests() + 1).mapToObj(
                    (i) -> DynamicTest.dynamicTest("Testni primer " + i,
                            () -> this.test(i)
                    )
            ).collect(Collectors.toList());
        }

    private void test(int i) {

        try {
            Naloga5.main(new String[] {aps.testFile("I", i), aps.testFile("T", i)});
            Naloga5Checker.test(aps.testFile("I", i), aps.testFile("T", i));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

