import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Naloga3Test {

	@Test
	public void testDLL() {
		Naloga3.DLL<Integer> dll = new Naloga3.DLL<Integer>();
		dll.append(1);
		dll.append(2);
		dll.append(3);
		dll.append(4);
		assertEquals(dll.popLast().intValue(), 4);
		assertEquals(dll.popFirst().intValue(), 1);
		assertEquals(dll.popLast().intValue(), 3);
		assertEquals(dll.popFirst().intValue(), 2);
	}

	@Test
	public void test1() {
		final String in = "Testni primer";
		Naloga3.input = in;
		Naloga3.kodiraj1();
		Naloga3.dekodiraj1();
		assertEquals(Naloga3.input, in);
	}

	@Test
	public void test2() {
		final int[] in = new int[] {20,30,43,45,39,34,50,16,42,34,38,30,42};
		Naloga3.tabela1 = in;
		Naloga3.N = 3;
		Naloga3.kodiraj2();
		Naloga3.dekodiraj2();
		assertArrayEquals(Naloga3.tabela1, in);
	}

	@Test
	public void testAll() {
		final String in = "Testni primer";
		Naloga3.input = in;
		Naloga3.N = 3;
		Naloga3.P = -1;
		Naloga3.K = 4;

		Naloga3.kodiraj();
		Naloga3.dekodiraj();
		assertEquals(Naloga3.input, in);
	}


	@TestFactory
	Collection<DynamicTest> testniPrimeri() {
		APSTest aps = new APSTest(Naloga3.class, "./Testni primeri/3/", 3);
		aps.ext = ""; // Zakaj te testne datoteke nimajo končnice? Who the fuck knows?!?
		return aps.genTests();
	}

}
