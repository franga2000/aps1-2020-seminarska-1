import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

public class Naloga4Test {
    Naloga4 mem;

    @BeforeEach
    public void beforeEach() {
        mem = new Naloga4();
        mem.init(1024);
    }
    
    @Test
    public void findBlockTest() {
        mem.firstBlock = new Naloga4.Block(null, 42, 0, 64, null);
        mem.firstBlock.next = new Naloga4.Block(mem.firstBlock, 84, 65, 1024, null);
        assertEquals(mem.firstBlock, mem.findBlock(42));
        assertEquals(mem.firstBlock.next, mem.findBlock(84));
        assertNull(mem.findBlock(69));
    }

    @Test
    public void freeTest() {
        mem.firstBlock = new Naloga4.Block(null, 42, 0, 64, null);
        mem.firstBlock.next = new Naloga4.Block(mem.firstBlock, 84, 65, 1024, null);
        mem.free(84);
        assertNull(mem.findBlock(84));
    }

    @Test
    public void allocTest() {
        assertTrue(mem.alloc(512, 1));
        assertEquals(0, mem.firstBlock.from);
        assertEquals(511, mem.firstBlock.to);

        assertTrue(mem.alloc(256, 2));
        assertEquals(512, mem.firstBlock.next.from);
        assertEquals(767, mem.firstBlock.next.to);

        assertFalse(mem.alloc(257, 3));
        assertTrue(mem.alloc(256, 3));
    }

    @Test
    public void allocFreeAllocTest() {
        // Fill up the memory
        assertTrue(mem.alloc(512, 1));
        assertTrue(mem.alloc(256, 2));
        assertTrue(mem.alloc(256, 3));

        assertFalse(mem.alloc(256, 4));
        mem.free(2);
        assertTrue(mem.alloc(256, 4));

        // Gap before first block test case
        mem.free(1);
        assertTrue(mem.alloc(512, 1));

        // Last block test case
        mem.free(3);
        assertTrue(mem.alloc(256, 3));
    }

    @Test
    public void defragTest() {
        mem.alloc(128, 1);
        mem.alloc(128, 2);
        mem.alloc(128, 3);
        mem.alloc(256, 4);

        mem.free(2);

        assertEquals(256, mem.findBlock(3).from);
        mem.defrag(1);
        assertEquals(128, mem.findBlock(3).from);
    }

    @Test
    public void toStringAfterAllocTest() {
        assertTrue(mem.alloc(512, 0));
        assertEquals("0,0,511", mem.firstBlock.toString());
    }


	@TestFactory
	Collection<DynamicTest> testniPrimeri() {
        APSTest aps = new APSTest(Naloga4.class, "./Testni primeri/4/", 4);
        return aps.genTests();
	}
}
